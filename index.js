/*
a) Log your information (names, institution, and Github username) into the console log with a statement that introduces you,
    e.g. “My name is Tsakane, a student from Xawe University of Technology and my Github username is ”
*/

function myInfo(name,institution,gitUsername)
{
    console.log("My name is " + name + ", a student from " + institution + " and my Github username is " + gitUsername);
}

myInfo("Terry","Tshwane University of Technology","TerryB26");

/*
b) Roll a dice and help predict which category will win the MTN Business App of the Year in the year 2022.
*/

var dice = Math.floor(Math.random(1) * 13);

function winnerPredictor(diceValue)
{
    var category;

    switch (diceValue)
    {
        case 1:
            category = "Best Consumer Solution";
            break;
        case 2:
            category = "Best Enterprise Solution";
            break;
        case 3:
            category = "Best African Solution";
            break;
        case 4:
            category = "Best Innovative Solution";
            break;
        case 5:
            category = "Best Gaming Solution";
            break;
        case 6:
            category = "Best Health Solution";
            break;
        case 7:
            category = "Best Agricultural Solution";
            break;
        case 8:
            category = "Best Educational Solution";
            break;
        case 9:
            category = "Best Financial Solution";
            break;
        case 10:
            category = "Best Hackathon Solution";
            break;
        case 11:
            category = "Best South African Solution";
            break;
        case 12:
            category = "Best Campus Cup Solution";
            break;
    }

    console.log("The winning category might be: " + category);
}

winnerPredictor(dice);

/*
c) Create, open, and read from a text file: Text content of an app that won the Best Campus Cup in 2021, its developer, category, and institution represented.
*/